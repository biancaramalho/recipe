# Tasks To Do

### Remove and add tasks to control your day

This project was built with JavaScript and simulated by json-Server.

### Description

This project is simple, it allows you to add and remove tasks according to your urgent need.

## Installation

###### clone the repository
$ git clone [https://gitlab.com/biancaramalho/recipe.git](https://gitlab.com/biancaramalho/recipe.git)

###### go into app's directory
$ cd TasksToDo

###### install app's dependencies
$ npm install

## Usage

`serve at localhost:8080 to view in the browser`
`$ npm start`

## Simulate with json-server

`$ json-server --watch db.json`

### Learn more

* [Json-Server](https://github.com/typicode/json-server)
* [NPM](https://github.com/npm/cli)
