import { editRow } from "../scripts/components/row/editRow";

const url = 'http://localhost:3000/tasks';

// GET REQUEST

export async function getTasks() {
    return fetch(url)
        .then(response => response.json())

}

// POST REQUEST
export async function addTask(data) {
    return fetch(url, {
        method: 'POST',
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json'
        },
    }).then(response => response.json());
}

// EDIT REQUEST
export async function editTask(id, title, description, priority) {
    return fetch(`${url}/${id}`, {
        method: 'PATCH',
        body: JSON.stringify({
            id,
            title,
            description,
            priority,
        }),
        headers: {
            'Content-Type': 'application/json'
        },
    }).then(response => response.json())
}

// DELETE REQUEST
export async function deleteTask(ID) {
    return fetch(`${url}/${ID}`, {
  method: 'DELETE',
})
.then(response => response.json()) 
.then(response => console.log(response))
}