import { appendRow } from './scripts/components/row/appendRow';
import { editRow } from './scripts/components/row/editRow';
import { addTask, getTasks, editTask } from './services/tasks';
import './style.scss';

async function save(event) {
        event.preventDefault();
    
        const title = document.getElementById('txtTitle')?.value;
        const description = document.getElementById('txtDescription')?.value;
        const priority = document.getElementById('addNum')?.value; 
        
        // const data = JSON.parse(dataStorage !== null ? dataStorage : '[]');
        
        const editIndex = document.querySelector('#hideId')?.value;
        if (editIndex) {
            // const ID = data[+editIndex].ID;
            // const row = { ID, title, description, priority };
            // data[+editIndex] = row;
            
            editTask(editIndex, title, description, priority)
            .then(row => {
                editRow(row);
            })
        } else {
            const row = { title, description, priority };
            addTask(row)
            .then(data => {
                appendRow(data);
                // data.push(row);
            })
        }
        
        
        $('#modalTask').modal('hide');
        $('#hideId').val(null);
        
        document.getElementById('txtTitle').value = null;
        document.getElementById('txtDescription').value = null;
        document.getElementById('addNum').value = null;
    }

// const storageKey = '__datas__';
// const dataStorage = localStorage.getItem(storageKey);
// localStorage.setItem(storageKey, JSON.stringify(data));

function populateTable() {
    getTasks()
    .then(data => {
        if (Array.isArray(data)) {
            for (const index in data) {
                if (Object.hasOwnProperty.call(data, index)) {
                    const item = data[index];
                    appendRow(item);
                }
            }
        }
    })

    // const data = JSON.parse(localStorage.getItem("__datas__") || '[]');
}

function addListeners() {
    const saveButton = document.querySelector("#btnSave");
    saveButton.addEventListener('click', save);
}

function index() {
    addListeners();

    populateTable();
}
index();