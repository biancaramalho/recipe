export function addStyles(
    _component,
    _styles,
) {
    if (Array.isArray(_styles)) {
        for (const style of _styles) {
            _component?.classList?.add(style);
        }
    }
}
