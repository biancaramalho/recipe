import { addStyles } from './addStyles';
import { setAtrributes } from './setAttributes';

export function createComponent(
    _elementName,
    _styles,
    _attributes,
) {
    const element = document.createElement(_elementName);

    addStyles(element, _styles);

    if(_attributes)
    setAtrributes(element, _attributes);

    return element;
}
