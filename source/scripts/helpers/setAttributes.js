export function setAtrributes(
    _component,
    _attributes,
) {
    if (Array.isArray(_attributes)) {
        for (const attribute of _attributes) {
            const [attributeName, attributeValue] = attribute;
            _component.setAttribute(attributeName, attributeValue);
        }
    }
}
