export default function addLinha(item){

    // Declaring variables
    let tr = document.createElement("tr");
    let tdId = document.createElement("td");
    let tdTitle = document.createElement("td");
    let tdDescription = document.createElement("td");
    let tdPriority = document.createElement("td");
    let tdAction = document.createElement("td");
    let buttonEdit = document.createElement("button");
    let buttonDel = document.createElement("button");
    let iconeEdit = document.createElement("i");
    let iconeDel = document.createElement("i");

    // Declaring tag classes
    buttonDel.classList.add("btn");
    buttonDel.classList.add("btn-danger");
    buttonEdit.classList.add("btn");
    buttonEdit.classList.add("btn-primary");
    iconeEdit.classList.add("fa");
    iconeEdit.classList.add("fa-edit");
    iconeDel.classList.add("fa");
    iconeDel.classList.add("fa-trash");
    
    // Add id
    buttonDel.id = "btnDelete";
    iconeDel.id = item.ID;
    buttonEdit.id = "btnEdit";
    iconeEdit.id = item.ID;
    tdId.id = "id";
    
    buttonEdit.setAttribute("data-bs-toggle","modal");
    buttonEdit.setAttribute("data-bs-target","#modalTask");
    // Declaring button type
    buttonDel.type = "button";
    buttonEdit.type = "button";

    // Include text inseide the td  
    tdId.textContent = item.ID;
    tdTitle.textContent = item.title;
    tdDescription.textContent = item.description;
    tdPriority.textContent = item.priority;

    // Include children inside the new tags
    buttonDel.appendChild(iconeDel);
    buttonEdit.appendChild(iconeEdit);
    tdAction.appendChild(buttonDel);
    tdAction.appendChild(buttonEdit);

    console.log("tr: ",tr);
    console.log("tdId: ",tdId);
    console.log("tdTitle: ",tdTitle);
    console.log("tdDescription: ",tdDescription);
    console.log("tdAction: ",tdAction);

    tr.appendChild(tdId);
    tr.appendChild(tdTitle);
    tr.appendChild(tdDescription);
    tr.appendChild(tdPriority);
    tr.appendChild(tdAction);
    let tb = document.querySelector("#tblDatas");
    let tbody = tb.querySelector("tbody");
    tbody.appendChild(tr);
}
