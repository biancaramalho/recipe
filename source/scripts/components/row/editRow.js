export function editRow(row) {
    const {
        id: ID,
        title,
        description,
        priority,
    } = row;

    const titleCell = document.getElementById(`cell-title-${ID}`);
    const descriptionCell = document.getElementById(`cell-description-${ID}`);
    const priorityCell = document.getElementById(`cell-priority-${ID}`);

    titleCell.innerText = title;
    descriptionCell.innerText = description;
    priorityCell.innerText = priority;
}
