import { createRow } from "./createRow";

export function appendRow(
    data,
) {
    const tbody = document.querySelector('tbody');
    const tr = createRow(data);

    tbody.appendChild(tr);
}
