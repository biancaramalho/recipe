import { deleteTask } from '../../../services/tasks'

export function deleteRow(
    index,
) {
    deleteTask(index)
    .then(index => {
        const tr = document.querySelector(`tr#${index}`);
        return tr.remove();
    })
}
