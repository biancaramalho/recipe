import { deleteTask } from '../../../services/tasks';
import { createComponent } from '../../helpers/createComponent';

export function createRow(
    data,
) {
    const {
        id: ID,
        title: titleText,
        description: descriptionText,
        priority: priorityText,
    } = data;
    const tr = createTr(ID);

    const id = createTd(ID, { name: 'id', ID, }, ["id"]);
    const title = createTd(titleText, { name: 'title', ID, }, ["title"]);
    const description = createTd(descriptionText, { name: 'description', ID, }, ["description"]);
    const priority = createTd(priorityText, { name: 'priority', ID, }, ["priority"]);

    const actions = createActions(ID);

    tr.appendChild(id);
    tr.appendChild(title);
    tr.appendChild(description);
    tr.appendChild(priority);
    tr.appendChild(actions);

    return tr;
}

function createTr(index) {
    return createComponent('tr', [], [['id', `row-task-${index}`]]);
}

function createTd(
    text = "",
    data = null,
    classNames = [],
) {
    let idAttribute = null;
    if (data) {
        const {
            name,
            ID,
        } = data;
        idAttribute = [['id', `cell-${name}-${ID}`]];
    }

    const td = createComponent('td', classNames, idAttribute);
    if (text) {
        td.textContent = text;
    }
    return td;
}

function createActions(id) {

    const storageKey = '__datas__';

    const editIconStyles = ['fa', 'fa-edit',];
    const editButtonStyles = ['btn', 'btn-primary',];
    const editButtonAttributes = [
        ['type', 'button'],
        ['id', `edit-item-${id}`],
        ['data-edit-item', id],
    ];

    const editButton = createComponent('button', editButtonStyles, editButtonAttributes);
    const editIcon = createComponent('i', editIconStyles);

    editButton.appendChild(editIcon);
    editButton.addEventListener('click', function () {
        const dataEditItem = document.getElementById(`row-task-${id}`);
        let title = dataEditItem.querySelector('.title').innerText
        let description = dataEditItem.querySelector('.description').innerText
        let priority = dataEditItem.querySelector('.priority').innerText
        
        $('#modalTask').modal('show');
        document.getElementById('hideId').value = id;
        document.getElementById('txtTitle').value = title;
        document.getElementById('txtDescription').value = description;
        document.getElementById('addNum').value = priority;
    });

    const delIconStyles = ['fa', 'fa-trash',];
    const delButtonStyles = ['btn', 'btn-danger',];
    const delButtonAttributes = [
        ['type', 'button'],
        ['id', `del-item-${id}`],
        ['data-del-item', id],
    ];

    const delButton = createComponent('button', delButtonStyles, delButtonAttributes,);
    const delIcon = createComponent('i', delIconStyles);

    delButton.appendChild(delIcon);

    delButton.addEventListener('click', function () {
        const tr = document.getElementById(`row-task-${id}`);
        const dataDelItem = document.getElementById(`del-item-${id}`);
        deleteTask(id)
        .then(() => {
            
            tr.remove();
        })

    });

    const td = createTd();
    td.appendChild(editButton);
    td.appendChild(delButton);
    return td;
}
