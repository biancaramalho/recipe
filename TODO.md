# ASSIGNMENT UI ESSENTIALS CONTROL

### **TASKS FINISHED**

 - [x] Use a module bundler
 - [x] Use a CSS Framework
 - [x] Use some ES6 features 
     *   [x] Create table
     *   [x] Create dynamic rows in the table
     *   [x] Create delete button and its functions
     *   [x] Fix the loading error after clicking the delete button
     *   [x] Create edit button and its functions 
 - [x] Remove node_modules from the repository
     * [x] Node_modules removed with git
     * [x] Add .DS_Store in .gitignore file
 - [x] Horizontal bar removed
 - [x] Add space/margins between action btns

### **TASKS TO DO**

 - [ ] Use client-side rendering only
 - [ ] Use a REST API mock
 - [ ] Improve project instructions
 - [ ] Bug: add task fields coming filled
 - [ ] Remove build from the project's src
 - [ ] Remove location.reload()
 - [ ] Make labels clickable to select fields
 - [ ] Fetch data from any external URL

